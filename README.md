### 注意1：3.0版本启动若果出现SQL相关错误，删除用户目录下.redis_plus重新启动软件即可

# RedisPlus

#### 项目介绍

RedisPlus是为Redis可视化管理开发的一款开源免费的桌面客户端软件，支持Windows 、Linux、Mac三大系统平台，RedisPlus提供更加高效、方便、快捷的使用体验，有着更加现代化的用户界面风格。该软件支持单机、集群模式连接，同时还支持SSH（单机、集群）通道连接。RedisPlus遵循GPL-3.0开源协议，禁止二次开发打包发布盈利，违反必究！RedisPlus致力于为大家提供一个高效的Redis可视化管理软件。

#### 下载地址

百度下载：https://pan.baidu.com/s/1ETwWnEj4rbsE1S3GlYHlWg  (linux , windows, mac)


#### 版本说明

版本的命名规则以x.y.z形式

重大版本更新发布会更改x位主版本，例如v2.0.0 版本会增加集群、支持ssh通道连接、国际化等功能

一般解决了多个缺陷后，会发布一个小版本，更改y位，例如v1.1.0解决了1.0.0 的问题后发布的小版本

z位的更改不会发布版本，这是缺陷修复或者小需求增加的正常迭代


#### 版本规划

V4.0

0.重写软件骨架及布局

1.重写服务连接模块

2.重写服务数据操作

3.重写服务信息模块

4.重写服务配置模块

5.重写服务监控预警

6.重写软件日志模块

7.重写软件命令模式

8......

#### 软件交流

1.QQ群：857111033  点击链接加入群聊【RedisPlusj交流群】：https://jq.qq.com/?_wv=1027&k=5nFw9eg

2.issues专区：https://gitee.com/MaxBill/RedisPlus/issues
 

#### 技术选型

1.支持跨平台，使用openjdk11和openjfx12开发

2.使用derby内嵌数据库

3.使用springboot开发框架


#### 打包说明

1.linux平台：需要安装fakeroot、rpm、alien库

2.windows：需要安装WiX Toolset、Inno Setup、microsoft .net framework库

#### 应用截图

![主页](https://images.gitee.com/uploads/images/2019/0713/174106_5cd40f66_1252126.png "屏幕截图.png")